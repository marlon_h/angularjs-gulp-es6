const gulp = require('gulp');
const browserify = require('browserify');
const babelify = require('babelify');
const source = require('vinyl-source-stream');
const buffer = require('vinyl-buffer')

gulp.task('build', () => {
    let sources = browserify({
        entries: 'src/app/app.js'
    }).transform(babelify.configure({
        presets: ['es2015']
    }));

    return sources.bundle()
        .pipe(source('app.min.js'))
        //.pipe(buffer())
        .pipe(gulp.dest('dist'));
});
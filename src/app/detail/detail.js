//import angular from 'angular';
import detailComponent from './detail.component';

let detailModule = angular.module('detail', []);

detailModule.component('detail', detailComponent);

export default detailModule.name;